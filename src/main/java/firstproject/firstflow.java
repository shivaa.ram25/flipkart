package firstproject;

import java.io.InterruptedIOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class firstflow {

	@Test
	public void newflow() {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
	      driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	      driver.manage().window().maximize();
	      driver.get("https://www.flipkart.com/");
	    //driver.findElement(By.xpath("//button[text()='X']")).click();
	      driver.findElementByXPath("(.//*[normalize-space(text()) and normalize-space(.)='Back to top'])[1]/following::button[1]").click();
	     Actions  ac= new Actions(driver);
	     ac.moveToElement(driver.findElementByXPath("(//span[@class='_1QZ6fC _3Lgyp8'])[1]")).perform();
	  
	     driver.findElementByLinkText("Mi").click();
	     String actualTitle="Mi Mobiles";
		if(actualTitle.contains("Mi"))
	     {
	     System.out.println("Verification successful"+actualTitle);
	     }
	     else
	     {
	     System.out.println("Verification of page title failed"+"actual title is"+actualTitle);
	     }
		driver.findElementByXPath("//div[text()='Newest First']").click();
		try {
			Thread.sleep(3000);
		}
		catch (InterruptedException e) {
			
		}
		
	   List<WebElement> brand = driver.findElementsByXPath("//div[@class='col col-7-12']/div[1]");
	   List<WebElement> prices = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
	   for (int i = 0; i < brand.size(); i++) {
			System.out.println("The mobile model "+brand.get(i).getText()+" has a price of "+prices.get(i).getText());
		}
	   
	   driver.quit();
	    
	   
	   
}
}